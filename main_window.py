from ast import For
import os
from PySide2 import QtWidgets, QtCore, QtGui
from PySide2.QtCore import QObject, Signal, Slot
from datetime import datetime

from ToDo_api import ToDoApp
import uuid

class Ticket(QtWidgets.QWidget):
    
    update_supp_main = Signal(list)
    
    def __init__(self,
                 number_ticket,
                 title_ticket,
                 label_date_create_ticket,
                 edit_date_start_ticket,
                 edit_date_finish_ticket,
                 edit_description,
                 edit_status):
       
        QtWidgets.QWidget.__init__(self)
        self.id_ticket = QtWidgets.QLabel(number_ticket)
        self.title_ticket = QtWidgets.QLabel(title_ticket)
        self.label_date_create_ticket = QtWidgets.QLabel(label_date_create_ticket)
        self.edit_date_start_ticket  = QtWidgets.QLabel(edit_date_start_ticket)
        self.edit_date_finish_ticket = QtWidgets.QLabel(edit_date_finish_ticket)
        self.edit_description = QtWidgets.QLabel(edit_description)
        self.edit_status = QtWidgets.QLabel(edit_status)
        
        self.button_delete_ticket = QtWidgets.QPushButton("Delete Ticket")
        self.button_delete_ticket.clicked.connect(self.button_delete_clicked)
        #self.button_modif_ticket = QtWidgets.QPushButton("Modify Ticket")  
        #self.button_modif_ticket.clicked.connect(self.button_modify_clicked)
           
        self.layout_self = QtWidgets.QFormLayout()
        self.layout_self.addRow(self.tr("&ID Ticket: "), self.id_ticket)
        self.layout_self.addRow(self.tr("&Title: "),  self.title_ticket)
        self.layout_self.addRow(self.tr("&Date to create : "), self.label_date_create_ticket)
        self.layout_self.addRow(self.tr("&Date to start : "), self.edit_date_start_ticket)
        self.layout_self.addRow(self.tr("&Date to finish : "), self.edit_date_finish_ticket)
        self.layout_self.addRow(self.tr("&Description task : "), self.edit_description)
        self.layout_self.addRow(self.tr("&Status : "), self.edit_status)
        self.layout_self.addRow(self.button_delete_ticket)
        #self.layout_self.addRow(self.button_modif_ticket)
        
        self.groupBox = QtWidgets.QGroupBox("TICKET DAY'S YOLO ---> " + self.title_ticket.text())
        self.groupBox.setObjectName("myParentWidget")
        
        # Set the color ticket with status state
        if edit_status == 'Wait':
            print('WAIT')
            self.groupBox.setStyleSheet("QWidget#myParentWidget { border:1px solid #fa7e75; background-color:rgba(250, 126, 117, 50)}")
        elif edit_status == 'Progess':
            print('PROGRESS')
            self.groupBox.setStyleSheet("QWidget#myParentWidget { border:1px solid #f7ac32; background-color:rgba(247, 172, 50, 50);}")
        elif edit_status == 'Closed':
            print('CLOSE')
            self.groupBox.setStyleSheet("QWidget#myParentWidget { border:1px solid #aec9bd; background-color:rgba(174, 201, 189, 50);}")
        else:
            pass
       
        self.groupBox.setLayout(self.layout_self)
        vbox = QtWidgets.QHBoxLayout()
        vbox.addWidget(self.groupBox)
        # setting the minimum size
        self.setObjectName("Ticket")
        self.setLayout(vbox)

    def button_delete_clicked(self):
        # Check if json file get the id ticket and delete it
        id_ticket = self.id_ticket.text() 
        #print('Delete --> ',id_ticket)
        ToDoApp.remove_ticket(id_ticket) # process json file
        print(self)
        self.hide()
  
    def button_modify_clicked(self):
        # Check if json file get the id ticket open it and save it
        id_ticket = self.id_ticket.text()
        #print('Modif -->',id_ticket)
        ToDoApp.modify_ticket(id_ticket)
        self.hide()

class FormulaireTicket(QtWidgets.QDialog,QObject):
    
    
    update_add_main = Signal(list)
    
    def __init__(self,parent=None):

        QtWidgets.QDialog.__init__(self)
        self.setWindowTitle("ADD NEW TICKET IN THE TO-DO-LIST")
        self.id_ticket = QtWidgets.QLabel(str(uuid.uuid4()))
        self.title_ticket = QtWidgets.QLineEdit()
        self.label_date_create_ticket = QtWidgets.QLabel( datetime.today().strftime('%d/%m/%Y - %H:%M') )

        self._today_start_button = QtWidgets.QPushButton('&Today', clicked=self.setToday)
        self.edit_date_start_ticket = QtWidgets.QDateEdit()
        self.edit_date_start_ticket.setDate(QtCore.QDate().currentDate())
        self.edit_date_start_ticket.setCalendarPopup(True)
        self.edit_date_start_ticket.calendarWidget().layout().addWidget(self._today_start_button)
        
        self._today_finish_button = QtWidgets.QPushButton('&Today', clicked=self.setToday)
        self.edit_date_finish_ticket = QtWidgets.QDateEdit()
        self.edit_date_finish_ticket.setDate(QtCore.QDate().currentDate())
        self.edit_date_finish_ticket.setCalendarPopup(True)
        self.edit_date_finish_ticket.calendarWidget().layout().addWidget(self._today_finish_button)
        
        self.edit_description = QtWidgets.QTextEdit() 
        self.button_valid_ticket = QtWidgets.QPushButton("Validation")
        self.button_close_ticket = QtWidgets.QPushButton("Cancel")
        
        status_value = ['Wait','In Progess','Closed']
        self.edit_status = QtWidgets.QComboBox()
        self.edit_status.addItems(status_value) 
         
        self.layout_self = QtWidgets.QFormLayout() 
        self.layout_self.addRow(self.tr("&ID Ticket: "),self.id_ticket)
        self.layout_self.addRow(self.tr("&Title: "),self.title_ticket)
        self.layout_self.addRow(self.tr("&Date creation : "), self.label_date_create_ticket)
        self.layout_self.addRow(self.tr("&Date to start : "), self.edit_date_start_ticket)
        self.layout_self.addRow(self.tr("&Date to finish : "), self.edit_date_finish_ticket)
        self.layout_self.addRow(self.tr("&Description task : "), self.edit_description)
        self.layout_self.addRow(self.tr("&STATUS : "), self.edit_status)
        
        self.layout_horizontal = QtWidgets.QHBoxLayout()
        self.layout_horizontal.addWidget(self.button_valid_ticket)
        self.layout_horizontal.addWidget(self.button_close_ticket)
         
        self.layout_self.addRow(self.layout_horizontal)
        
        self.setLayout(self.layout_self)
        self.setWindowFlags(QtCore.Qt.WindowStaysOnTopHint)
        
        self.button_valid_ticket.clicked.connect(self.button_valid_clicked)
        self.button_close_ticket.clicked.connect(self.button_close)

    def setToday(self):
        today = QtCore.QDate().currentDate()
        self.edit_date_finish_ticket.calendarWidget().setSelectedDate(today)
        self.edit_date_start_ticket.calendarWidget().setSelectedDate(today)
        
    def button_valid_clicked(self):
        #Get all fields in self and send to save the data in json file
        data_ticket = {
            'ID' :  self.id_ticket.text(),
            'title' : self.title_ticket.text(),
            'date_create': self.label_date_create_ticket.text(),
            'date_start' :self.edit_date_start_ticket.text(),
            'date_finish' : self.edit_date_finish_ticket.text(),
            'description_task' : self.edit_description.toPlainText(),
            'status' : self.edit_status.currentText()
        }
        ToDoApp.add_ticket(data_ticket)
        # Create Ticket frame in MainWindow
        number_ticket = self.id_ticket.text()
        title_ticket = self.title_ticket.text()
        label_date_create_ticket = self.label_date_create_ticket.text()
        edit_date_start_ticket =  self.edit_date_start_ticket.text()
        edit_date_finish_ticket = self.edit_date_finish_ticket.text()
        edit_description = self.edit_description.toPlainText()
        status =  self.edit_status.currentText()
        new_ticket = Ticket(
                    number_ticket,
                    title_ticket,
                    label_date_create_ticket,
                    edit_date_start_ticket,
                    edit_date_finish_ticket,
                    edit_description,
                    status)
        
        self.update_add_main.emit([new_ticket])
        self.close()
        
    def button_close(self):
        self.close()
        
class MainWindow(QtWidgets.QWidget):
    
    def __init__(self, application):
        QtWidgets.QWidget.__init__(self)
        self.application = application

        self.setWindowFlags(QtCore.Qt.WindowStaysOnTopHint)
        self.setWindowTitle('TO DO LIST by Dym')  
        self.setGeometry(600, 100, 1000, 900)
          
        # Scrolling
        self.scroll = QtWidgets.QScrollArea(self)
        self.scroll.setWidgetResizable(True)
        self.scrollAreaWidgetContents = QtWidgets.QWidget()
        #self.scroll.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOn)
        #self.scroll.setHorizontalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)

        # Create VLayout Main
        self.VLayout_main = QtWidgets.QVBoxLayout() #<-- All Layout setting inside    
        # Create Button 'Open Formular Ticket'
        self.button_create = QtWidgets.QPushButton("Create Ticket") #<-- Btn to open form to input data Ticket
        # Action button
        self.button_create.clicked.connect(self.button_create_ticket)      
        #Create GridLayout
        self.grid_layout =  QtWidgets.QGridLayout(self.scrollAreaWidgetContents)  
        
        self.grid_layout.setAlignment(QtCore.Qt.AlignTop)
        
        # Add Button in VLayout Main
        self.VLayout_main.addWidget(self.button_create)      
        # Add Button in VLayout Main
        self.VLayout_main.addLayout(self.grid_layout)
     
        
           
        self.scroll.setWidget(self.scrollAreaWidgetContents)
        self.VLayout_main.addWidget(self.scroll)
        
        #-------------------------------- ASTUCE POUR AVOIR UNE GRID DYNAMIQUE--------------------------------
        self.grid_layout.addWidget(QtWidgets.QLabel(""),0,0)
        self.grid_layout.addWidget(QtWidgets.QLabel(""),0,1)
        self.grid_layout.addWidget(QtWidgets.QLabel(""),0,2)
        #self.grid_layout.addWidget(QtWidgets.QLabel(""),0,3)
        #self.grid_layout.addWidget(QtWidgets.QLabel(""),0,4)
        #self.grid_layout.addWidget(QtWidgets.QLabel(""),0,5)

        # Add Main Vertical Layout in Application
        #self.widget.setLayout(self.VLayout_main)
        self.setLayout(self.VLayout_main)
        
        # Get list data Ticket from Json File
        list_saved_tickets = self.GetListSavedTickets()
        for ticket in list_saved_tickets:      
            self.grid_layout.addWidget(ticket)
        
        self.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
        self.customContextMenuRequested.connect(self.on_context_menu)
        
        
    def on_context_menu(self, pos):
        context = QtWidgets.QMenu(self)
        context.addAction(QtWidgets.QAction("test 1", self))
        context.addAction(QtWidgets.QAction("test 2", self))
        context.addAction(QtWidgets.QAction("test 3", self))
        context.exec_(self.mapToGlobal(pos))
         
    def update_for_delete_ticket(self):
        self.ticket = Ticket(self.application)
        self.ticket.update_supp_main.connect(self.deleteinfos)
          
    def deleteinfos(self, liste):
        """Récupération des infos
        """
        self.grid_layout.removeWidget(liste[0])
        self.grid_layout.update()
        
    def button_create_ticket(self):
        #print('My button works... Create Ticket')
        self.formulaire_ticket = FormulaireTicket(self.application)
        self.formulaire_ticket.update_add_main.connect(self.recupinfos)
        self.formulaire_ticket.show()
        #self.dlg.exec_()
            
    def recupinfos(self, liste):
        """Récupération des infos
        """        
        self.formulaire_ticket.hide() # cache la fenêtre dialog encore affichée (elle sera fermée juste après)
        self.grid_layout.addWidget(liste[0])
        
    def GetListSavedTickets(self):
        # Check if json exist before add ticket in the board
        DIR_JSON = os.path.join(os.path.dirname(__file__), 'json_data.json')
        
        list_tickets = []
        if os.path.exists(DIR_JSON): 
            print('******************* GET SAVED DATA *********************')
            # Add all existing Ticket in the MainWindows
            data_from_json = ToDoApp.get_saved_tickets()
            print('********************************************************')
            
            for element in data_from_json['Data']:
                # Create Ticket frame in MainWindow
                number_ticket =  element.get("ID")
                title_ticket = element.get("title")
                label_date_create_ticket = element.get("date_create")
                edit_date_start_ticket =  element.get('date_start')
                edit_date_finish_ticket = element.get('date_finish')
                edit_description = element.get('description_task')
                status =  element.get("status")
                
                # Create ticket
                ticket = Ticket(
                    number_ticket,
                    title_ticket,
                    label_date_create_ticket,
                    edit_date_start_ticket,
                    edit_date_finish_ticket,
                    edit_description,
                    status)
                
                # Add result in the tickets list
                list_tickets.append(ticket)
                     
        return list_tickets
    
