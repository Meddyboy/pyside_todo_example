# ToDo Application with PySide and Json File

This ToDo application has been developed using PySide2 and allows you to manage a to-do list with a simple graphical interface.

## Features

1. **Add a Task:** Allows you to add a new task to the list.

2. **Remove a Task:** Removes a task from the list based on its ID.

3. **Modify a Task:** Opens a form with existing data for modification.

## Usage

1. Make sure you have Python installed on your machine.

2. Install the dependencies by running the following command:
   ```bash
   pip install PySide2
   ```
# Run the application using the command:
   ```bash
   python __main__.py
   ```
The application automatically loads data from the existing JSON file if it exists.

## Project Structure

    __main__.py: The main file of the application.
    main_window.py : model pyside and user interface
    ToDo_api.py : function to modify json file 'json_data.json'
    json_data.json: The JSON file used to store task data.

