from PySide2 import QtWidgets
import json
import os

DIR_JSON = os.path.join(os.path.dirname(__file__), 'json_data.json')

class ToDoApp:
    
    # Read json file when load app
    def get_saved_tickets(filename=DIR_JSON, parent=None):
        '''
        Read json file and return the data.
        '''
        if os.path.exists(filename):    
            print("The `TO_DO_LIST` Json file found.")
            with open(filename, "r+") as outfile:
                # First we load existing data into a dict (old data).
                file_data = json.load(outfile)
                print('Get data from file -->', file_data)
            return file_data  
        else:
            print("Json file not found.")

    # Add new Ticket
    def add_ticket(data, filename=DIR_JSON):
        '''
        Add a new ticket to the json file.
        '''
        # Check if json file exists
        if not os.path.exists(filename):         
            # Set Dict for new json file
            data_dict = dict({'Data':[]})     
            # Append data in data_dict
            data_dict['Data'].append(data)    
            # Serializing json
            json_object = json.dumps(data_dict, indent=4)          
            # Writing to sample.json
            with open(filename, "w") as outfile:
                outfile.write(json_object)
                print('WOoooow Yolo, The Works is waiting for you Baby!!!')           
        else:
            with open(filename, "r+") as outfile:
                # First we load existing data into a dict (old data).
                file_data = json.load(outfile)
                print('Get data from file -->' ,file_data)          
                # Get data from inputs (new data)
                print('Get data from inputs -->' ,data)           
                # Join new_data with file_data inside emp_details
                file_data['Data'].append(data)
                print('Get data from Ui -->' ,file_data)              
                # Sets file's current position at offset.
                outfile.seek(0)            
                # convert back to json.
                json.dump(file_data, outfile, indent=4)
        
    # Remove Ticket
    def remove_ticket(id_ticket):
        '''
        Remove a ticket with a given ID from the json file.
        '''
        print('Jusqu a la vie!!')
        saved_tickets = ToDoApp.get_saved_tickets()
        max_ticket = len(saved_tickets['Data'])                                       
        for i in range(max_ticket-1):   
            if saved_tickets['Data'][i]['ID'] == id_ticket:
                print('Remove ! -->',i)
                print('Remove ! -->',id_ticket)
                print('Remove ! -->',saved_tickets['Data'][i])
                print('Remove ! -->',type(saved_tickets))
                print('Remove ! -->',saved_tickets)
                del saved_tickets['Data'][i]
                print('Remove ! -->',saved_tickets)
         
        # Output the updated file with pretty JSON                                      
        open(DIR_JSON, "w").write(json.dumps(saved_tickets, sort_keys=True, indent=4, separators=(',', ': ')))
      
    
    # Modify Ticket
    def modify_ticket(id_ticket):
        '''
        Modify a ticket with a given ID.
        Open a form with the existing data for modification.
        '''
        saved_tickets = ToDoApp.get_saved_tickets()  
        max_ticket = len(saved_tickets['Data'])                                       
        for i in range(max_ticket):    
            if saved_tickets['Data'][i]['ID'] == id_ticket:
                print('Modify ! -->',id_ticket)
                # Open formulaire here with datas
