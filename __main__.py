import sys
import logging
from PySide2 import QtWidgets
from main_window import MainWindow


class ToDoApp:
    
    def __init__(self, args):
        self.pyside_app = QtWidgets.QApplication(args)
        self.main_window = MainWindow(application=self)
    
    def run(self):
        self.main_window.show()
        return(self.pyside_app.exec_())


if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO)
    application = ToDoApp(sys.argv)
    sys.exit(application.run())